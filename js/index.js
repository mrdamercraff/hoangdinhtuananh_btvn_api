const BASE_URL ='https://635f4b1a3e8f65f283b01253.mockapi.io'
var idEdited =null;

function fetchAllTodo(){
    turnOnLoading();
    axios({
        url:`${BASE_URL}/cloneTodos`,
        method: "GET",
    })
    .then(function(res){
        console.log('res: ', res.data);
        renderTodoList(res.data);
        turnOffLoading();
        
    })
    .catch(function(err){
        console.log('err: ', err);
        turnOffLoading();
    })
}
fetchAllTodo();

    

function removeTodo(idTodo){
    turnOnLoading();
    axios({
        url: `${BASE_URL}/cloneTodos/${idTodo}`,
        method:"DELETE",
    })
    .then(function(res){
        console.log('res: ', res);
       
        turnOffLoading();
        fetchAllTodo();
        
    })
    .catch(function(err){
        console.log('err: ', err);
        turnOffLoading();

    })
}
turnOnLoading();
function editTodo(idTodo){
axios({
    url:`${BASE_URL}/cloneTodos/${idTodo}`,
    method: "GET"
})
.then(function(res){
    console.log('res: ', res);
    turnOffLoading()
    document.getElementById('formName').value = res.data.cloneName;

    document.getElementById('formDesc').value = res.data.cloneDesc
    ;
    idEdited = res.data.id;

})
.catch(function(err){
    console.log('err: ', err);
    turnOnLoading();
    
});


}

function addTodos(){
var data =layThongTinTuForm();
var newTodo={
cloneName: data.cloneName,
cloneDesc: data.cloneDesc,
cloneCheck: true,
}
turnOnLoading();
axios({
    url: `${BASE_URL}/cloneTodos`,
    method:"POST",
    data: newTodo,
})
.then(function(res){
    turnOffLoading();
    fetchAllTodo();
})
.catch(function(err){
    turnOffLoading();
})
}

function updateTodos(){
    var data =layThongTinTuForm();
    turnOnLoading();
    axios({
        url:`${BASE_URL}/cloneTodos/${idEdited}`,
        method: "PUT",
        data: data, 
    })
    .then(function(res){
        turnOffLoading();
        fetchAllTodo();
    })
    .catch(function(err){
        turnOffLoading();
    })
}

