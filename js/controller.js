function renderTodoList(todos){
    var contentHTML ='';
    todos.forEach(function(item){
        var content=`<tr>
        <td>${item.id}</td>
        <td>${item.cloneName}</td>
        <td>${item.cloneDesc}</td>
        <td><input type="checkbox" ${item.cloneCheck ? "checked" : ""} />
        </td>
        <td><button class='btn btn-danger' onclick="removeTodo(${item.id})">Delete</button>
        <button class='btn btn-primary' onclick="editTodo(${item.id})">Edit</button>
        </td>
        </tr>`
        contentHTML +=content;
    })
  document.getElementById('tbody-todos').innerHTML = contentHTML;
}

function turnOnLoading(){
    document.getElementById('loading').style.display ='flex';
}
function turnOffLoading(){
    document.getElementById('loading').style.display ='none';
}
function layThongTinTuForm(){
    var name = document.getElementById('formName').value;
    var desc = document.getElementById('formDesc').value;
   return{
    cloneName: name,
    cloneDesc: desc,
   }
}